# Release Notes Vmedis
* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [1.8.0.6] - 2018-08-08
## Menu Data Jurnal Keuangan
- **Changed :**
	1. Ketika menambah jurnal keuangan, input Akun Kredit dipindah sebelum Akun Debit. Supaya lebih mudah menentukan "dari akun mana - ke akun mana".

## Menu Laporan Buku Besar
- **Removed :**
	1. Hapus informasi total mutasi pada footer laporan.